using System;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace YuuMod
{
    class YuuMod : Mod
    {
        internal static YuuMod instance;
        internal bool calamityLoaded;
        internal bool imkSushiLoaded;
        public YuuMod()
        {

            Properties = new ModProperties()
            {
                Autoload = true,
                AutoloadGores = true,
                AutoloadSounds = true
            };
        }
        public override void Load()
        {
            instance = this;


        }
        public override void PostSetupContent()
        {
            try
            {
                calamityLoaded = ModLoader.GetMod("CalamityMod") != null;
                imkSushiLoaded = ModLoader.GetMod("imkSushisMod") != null;
            }
            catch (Exception e)
            {
                ErrorLogger.Log("YuuMod PostSetupContent Error: " + e.StackTrace + e.Message);
            }
        }

        public override void AddRecipes()
        {
            if (!instance.imkSushiLoaded)
            {
                ModRecipe recipe = new ModRecipe(this);
                recipe.AddIngredient(ItemID.CopperBar);
                recipe.AddTile(TileID.Furnaces);
                recipe.SetResult(ItemID.TinBar);
                recipe.AddRecipe();

                recipe = new ModRecipe(this);
                recipe.AddIngredient(ItemID.TinBar);
                recipe.AddTile(TileID.Furnaces);
                recipe.SetResult(ItemID.CopperBar);
                recipe.AddRecipe();

                recipe = new ModRecipe(this);
                recipe.AddIngredient(ItemID.IronBar);
                recipe.AddTile(TileID.Furnaces);
                recipe.SetResult(ItemID.LeadBar);
                recipe.AddRecipe();

                recipe = new ModRecipe(this);
                recipe.AddIngredient(ItemID.LeadBar);
                recipe.AddTile(TileID.Furnaces);
                recipe.SetResult(ItemID.IronBar);
                recipe.AddRecipe();

                recipe = new ModRecipe(this);
                recipe.AddIngredient(ItemID.SilverBar);
                recipe.AddTile(TileID.Furnaces);
                recipe.SetResult(ItemID.TungstenBar);
                recipe.AddRecipe();

                recipe = new ModRecipe(this);
                recipe.AddIngredient(ItemID.TungstenBar);
                recipe.AddTile(TileID.Furnaces);
                recipe.SetResult(ItemID.SilverBar);
                recipe.AddRecipe();

                recipe = new ModRecipe(this);
                recipe.AddIngredient(ItemID.GoldBar);
                recipe.AddTile(TileID.Furnaces);
                recipe.SetResult(ItemID.PlatinumBar);
                recipe.AddRecipe();

                recipe = new ModRecipe(this);
                recipe.AddIngredient(ItemID.PlatinumBar);
                recipe.AddTile(TileID.Furnaces);
                recipe.SetResult(ItemID.GoldBar);
                recipe.AddRecipe();

                recipe = new ModRecipe(this);
                recipe.AddIngredient(ItemID.DemoniteBar);
                recipe.AddTile(TileID.Furnaces);
                recipe.SetResult(ItemID.CrimtaneBar);
                recipe.AddRecipe();

                recipe = new ModRecipe(this);
                recipe.AddIngredient(ItemID.CrimtaneBar);
                recipe.AddTile(TileID.Furnaces);
                recipe.SetResult(ItemID.DemoniteBar);
                recipe.AddRecipe();
            }
        }
    }
}

